.. burningbrain documentation master file, created by
   sphinx-quickstart on Tue May 25 18:04:11 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BurningBrain Dokumentation
==========================

.. image:: images/img/cover-index.jpg

-----







.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: ABOUT

   dreamsys/dreamsys
   dreamsys/burningbrain



.. toctree::
   :maxdepth: 2
   :numbered:
   :caption: *** INFORMATION ***

   info/roadmap
   info/release 
   info/bugfix


.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: *** WISSENSDATENBANK ***

   application/index  
   infrastructure/index   


.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: *** ERSTE SCHRITTE ***

   quickstart/index


.. toctree::
   :maxdepth: 3
   :numbered:
   :caption: *** BEISPIELE UND HILFEN ***

   help/index   



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
