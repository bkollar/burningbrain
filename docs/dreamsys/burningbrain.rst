BURNINGBRAIN
============

.. important:: 
    
    Da BurningBrain momentan ein Lern.- und Testprojekt ist, unterliegt es starken Änderungen.
    Jeder Bereich kann sich täglich ändern und neu Varianten, Visualisierungen und Funktionalitäten aufweisen.

.. raw:: html

    <iframe width="100%" height="400" src="https://www.youtube.com/embed/2Iph2l3RWmk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
 

 
Kurzer Film über die Alpha-Version des Editors. Verzeihen Sie bitte die Hintergrundmusik ;-)
 

-----
 
.. image:: /images/img/editor.png
 

-----