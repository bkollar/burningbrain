DREAMSYS
========


.. image:: /images/img/dreamsys.jpg


.. note::

    Dreamsys ist ein Label, unter dem Software entwickelt wird. 
    Dreamsys gibt es bereits seit 1994. Die Applikation UnityCs-Majime ist basierend auf der technologie der AWS-Cloud, und unter DreamSys entstanden. 
    Der JavaFX Client befüllt in den einzelnen Regionen der AWS Cloud, Mongo-Datenbanken und nutzt diverse Fabric-Services.
    UnityCS wurde leider aus Kapazität.-, Kostengründen und Verteilungsproblemen 2019 eingestellt. 
 

UnityCS ist vollumfänglich, in mehr 7000 
Arbeitsstunden, von mir erstellt. 

 


.. raw:: html

    <a href="https://majimedoc.readthedocs.io/en/latest/index.html" target="_blank">Majime Documentation</a><br/>
    <a href="https://youtu.be/-w8Nuow6FdA" target="_blank">Video : (UnityCS) Majime/UnityCS all</a><br/>
    <a href="https://youtu.be/o8qZtuW413w" target="_blank">Video : (UnityCS) Majime/UnityCS Comments</a><br/><br/>


 
Heute dient Dreamsys als Lernplattform und Projekt-Prototyping.  

-----


OWNER
^^^^^    

.. image:: /images/img/bruno.png

| Bruno Kollar  
| Application Developer | DevOps Engineer
| bkollar@dreamsys.ch


.. note::
    Seit 1989 habe ich für die Crossmedia Publishing Welt die ersten Katalog Produktionstools erstellt.
    Die Reisebranche wie Hotelplan Imholz gehörte dazu.
    13 Jahre habe ich für die Lebensmittelindustrie an einem Verpackungssystem für die Migros unter den strengen Richtlinien von GMP (GAMP 5) entwickelt.
    Mit grosser Freude entwickle ich und bleibe mit Ausdauer, Innovation, Fleiss und grosser Interesse dabei. 
    In den letzten Jahren habe ich mich vermehrt mit Azure.-, AWS-Infrastruktur auseinander gesetzt. 
    Dazu gehörten auch DevOps Arbeiten. 


-----

.. ENTWICKLUNG
.. ^^^^^^^^^^^

.. #. JavaFX/Java
.. #. Python/Django/Flask/Jinja/html/Javascript,... 
.. #. Golang
.. #. C#/WPF

.. -----

.. FRAMEWORK
.. ^^^^^^^^^^
.. #. Terraform
.. #. Azure Cli
.. #. Ansible
.. #. React
.. #. Elektron

.. -----

.. INFRASTRUKTUR
.. ^^^^^^^^^^^^^
.. #. Azure Cloud (Medium)
.. #. Azure Devops (Full)
.. #. AWS (Rout53, Hosting der Domains von Dreamsys)  
.. #. AWS (S3, S3,Glacier, Lightsail, CodeCommit, SNS, WorkDocs, WorkMail, Poly)

.. -----

FRAGEN
^^^^^^
| Gerne stehe ich für Fragen betreffen BurningBrain zur Verfügung
| mailto:bkollar@dreamsys.ch
