
REGISTRIERUNG
=============



.. important:: 
    
   Für eine Registrierung benötigt es 2 Schritte.
      | 1. Lizenzschlüssel anfordern
      | 2. Registrierung mit dem Lizenzschlüssel ausführen.
      | 3. Anschliessend kann eine Login ausgeführt werden.


LIZENSIERUNG
^^^^^^^^^^^^

.. note:: 

   Mit dem Lizenzkey erhalten Sie den Schlüssel für Ihre Registrierung.
   Sie benötigen den Key einmal für die Registrierung. Danach können sie diesen löschen.
   Der Key muss nicht aufbewahrt werden.

.. raw:: html

   <a href="https://burningbrain.ch/buy" target="_blank">Hole Dir einen Lizenz-Key</a><br/><br/>
  

.. image:: /quickstart/images/mail_licence_key.jpg
   :width: 400px


-----

REGISTRIERUNG
^^^^^^^^^^^^^

.. note:: 

   Bei der Registrierung in BurningBrain wird ein Lizenzschlüssel benötigt, auch wenn sie einen "Free Plan" Account benutzen.

.. raw:: html

   <a href="https://burningbrain.ch/buy" target="_blank">Registriere Dich hier</a><br/><br/>
  

.. image:: /quickstart/images/mail_register.jpg
   :width: 400px

-----


LOGIN
^^^^^

.. important:: 

   Derzeit ist das Anmelden auf das "Self Hosted Login" begrenzt
   Die Anmeldungen via anderen Provider ist in der Bearbeitung. 


.. raw:: html

   <a href="https://burningbrain.ch/login" target="_blank">Melde Dich hier an</a><br/><br/>
  

.. image:: /quickstart/images/login.jpg
   :width: 400px


-----

PASSWORT RESET
^^^^^^^^^^^^^^

.. note:: 

   Um sich ein neues Passwort zu setzen, muss du das alte Passwort nicht kennen.
   Durch das Zusenden eines Token kann das neu setzen des Passwortes einfach durchgeführt werden.


.. raw:: html

   <a href="https://burningbrain.ch/resetlogin" target="_blank">Setze dein Passwort zurück</a><br/><br/>

.. image:: /quickstart/images/login.jpg
   :width: 400px

-----

.. note:: 

   Ein E-Mail mit einem Token wird zugesendet. 
   Nutze den Token um das Passwort neu zu setzen.


.. image:: /quickstart/images/token.jpg
   :width: 400px

-----

.. note:: 

   Füge den Token in das Formular für die Passwortänderung hinzu.
   Nach dem absendes des Formulars, kann der Benutzer sich neu anmelden.


.. image:: /quickstart/images/reset.jpg
   :width: 400px


