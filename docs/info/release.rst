
RELEASE
=======

Released
^^^^^^^^

#. [Released] Create Licence Key and managed this via Sendgrid-Mail (Licence Key )
#. [Released] Create Registration and managed this via Sendgrid-Mail (Account Free)
#. [Released] Login (selfhostet) and managed this via Sendgrid-Mail (Token)
#. [Released] Index Page: Card-Development Information
#. [Released] Editor: Alpha Version of Object-Editor 



Next Release
^^^^^^^^^^^^

Date: ``coming soon``

#. Design User Page
#. Design Dashboard Page
#. Design About Page
#. Design Info
