
ROADMAP
=======


.. important:: 
    
    | Alle Ziele werden im BurningBrain Team definiert. 
    | Wir verfolgen unser Ziel und planen in jeder Sprint-Periode die neuen Tasks.
    | - Release werden derzeit individuell ausgeführt.
    | - Bugfix werden jederzeit ausgeführt.



In Planung
^^^^^^^^^^

#. Project: Base KI
#. Project: Base Graphs



In der Pipeline
^^^^^^^^^^^^^^^
#. Project: Base Roule: Innovative human thinking
#. Project: Base Ability to learn
#. Google Login
#. Microsoft Login


    
Neu aufgenommen
^^^^^^^^^^^^^^^
#. Recursive Änderungen für Elemente
#. Change Color, Border-Size, Border-Color, Shadow, Rotate, Delete


    
