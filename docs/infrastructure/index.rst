
INFRASTRUKTUR
=============

.. raw:: html
    
    <div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/bb328d4c-02eb-4cb9-8768-9ed1845e1b58" id="SqmZ1kf20M~D"></iframe></div>

.. toctree::
    :maxdepth: 2

    azure
    ki
    language
