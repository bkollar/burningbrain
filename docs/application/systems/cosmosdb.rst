COSMOS-DB
=========


.. note:: 
    
    Die Azure-CosmosDB wir für BurningBrain im MongoDB Modus betrieben.
    Azure Cosmos DB ist ein vollständig verwalteter NoSQL-Datenbankdienst für die moderne App-Entwicklung. 
    Es stehen garantierte Antwortzeiten im einstelligen Millisekundenbereich und eine Verfügbarkeit von 99,999 Prozent zur Verfügung, die durch SLAs, 
    die automatische und sofortige Skalierbarkeit und Open-Source-APIs für MongoDB und Cassandra sichergestellt werden.


    