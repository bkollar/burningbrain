BINDING-ELEMENTS
================

.. note:: 
    
    Mit dem Designer erstellst du Datengebundene Elemente. Diese Elemente können verschiedene 
    Bindings und provider haben. KI-Provider, URL-Pars-Provider usw. 


.. raw:: html

    <div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/cf62f810-678c-4064-b856-19d02a77ece3" id="t_.0vR.TzyLg"></iframe></div>
