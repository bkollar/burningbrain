EDITORS
=======

.. note:: 
    
    Mit dem Designer werden Brainstorming-Boards erstellt,
    die ein elementares Wissen bilden und visualisieren können. 



| Zoom = CRTL + Mouse-Scroll
| Move = Click background and move-mouse


.. raw:: html

    <div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/03985c94-ec38-4ffb-a41a-71816759c80d" id="sHUYGXU-yGv~"></iframe></div>


.. raw:: html

    <div style="width: 640px; height: 480px; margin: 10px; position: relative;"><iframe allowfullscreen frameborder="0" style="width:640px; height:480px" src="https://lucid.app/documents/embeddedchart/37390ef2-4957-4adc-9d17-c44e53e59e5b" id="eP7ZlpfGHhX_"></iframe></div>



