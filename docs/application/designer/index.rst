DESIGNER
========

.. note:: 
    
    Mit dem Designer können Datengebundene Elemente erstellt werden. Diese Elemente können verschiedene 
    Bindings und Provider nutzen. Die KI-Provider, URL-Pars-Provider werden Bereitgestellt. Es folgen mehr. 


.. toctree::
    :maxdepth: 1

    ./editors
    ./binding-elements